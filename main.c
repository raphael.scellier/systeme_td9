#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>


int main()
{
    sem_t *s, *u, *m;
    s=(sem_t*)mmap(NULL, sizeof(sem_t),PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,0,0);
    u=(sem_t*)mmap(NULL, sizeof(sem_t),PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,0,0);
    m=(sem_t*)mmap(NULL, sizeof(sem_t),PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS,0,0);
    sem_init(s,1,0);
    sem_init(u,1,0);
    sem_init(m,1,2);
    
    if(fork()==0)
    {
        // bananette
        sem_wait(m);
        printf("debut bananette\n");
        sleep(2+random()%2);
        printf("fin bananette\n");
        
        //v3(s)
        sem_post(m);
        sem_post(s);sem_post(s);sem_post(s);
        
        exit(0);
    }
    if(fork()==0)
    {
        // fraisade
        sem_wait(m);
        sem_wait(s);
        printf("debut fraisade\n");
        sleep(1+random()%2);
        printf("fin fraisade\n");
        
        sem_post(m);
        sem_post(u);
        
        exit(0);
    }
    if(fork()==0)
    {
        // anchoise
        sem_wait(m);
        sem_wait(s);
        printf("debut anchoise\n");
        sleep(2+random()%2);
        printf("fin anchoise\n");
        sem_post(m);
        sem_post(u);

        exit(0);
    }
    if(fork()==0)
    {
        // toulousaille
        sem_wait(m);
        sem_wait(s);
        printf("debut toulousaille\n");
        sleep(1+random()%2);
        printf("fin toulousaille\n");
        sem_post(m);
        sem_post(u);
        
        exit(0);
    }
    if(fork()==0)
    {
        // GB
        
        sem_wait(u);sem_wait(u);sem_wait(u);      
        sem_wait(m);   
        printf("debut gb\n");
        sleep(1+random()%2);
        printf("fin gb\n");
        
        sem_post(m);
        exit(0);
    }
    wait(NULL);wait(NULL);wait(NULL);wait(NULL);wait(NULL);
    sem_destroy(s);sem_destroy(u);sem_destroy(m);
    exit(0);
}